import Vue from 'vue'
import '@culqilabs/ui/dist/culqi-ui.min.css'

<% if (options.components) { %>
  import { <%=options.components.join(',') %> } from '@culqilabs/ui/dist/components'
  <% options.components.forEach((component) => { %>
    Vue.use(<%=component %>)
    <% }) %>
<% } else { %>
  import VueMaterial from '@culqilabs/ui'
  Vue.use(VueMaterial)
    <% } %>
